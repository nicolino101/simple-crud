<?php namespace application\config;

class Config
{    
    public static function get(string $type, string $driver = null) : array
    {
        $c = [
            "company" => [
                'info'     => [
                    "title"    => 'Simple Crud Application',
                    "slogan"   => 'MVC',
                    "logo"     => null,
                    "phones"   => [
                        'local'    => '702-555-1212',
                        'tollfree' => '800-427-7557',
                        'vanity'   => '1800-TestDrive'
                    ],
                    "email"    => 'td@crud.com',
                    "address1" => 'East Whothehellcares Street',
                    "address2" => 'nowhere',
                    "city"     => 'East Bumfuck',
                    "state"    => 'New Jersey',
                    "zip"      => '99999',
                    "country"  => "Egypt",
                    "coords"      => [
                        "lat"   => "900.3223",
                        "lon"   => "-342.234423"
                    ]
                ]
            ],
            "api" => [
                "google" => [
                    "api_key" => 'GOOGLE_API_KEY'
                ]              
            ],
            "db"         => [
                "default"         => [
                    'driver'      => 'pdo_mysql',
                    'host'        => "127.0.0.1",
                    'dbname'      => "crud",
                    'username'    => "USER",
                    'password'    => "PASS",
                    'port'        => "3306",
                    'sshkey'      => []
                ],
                "crud"         => [
                    'driver'      => 'pdo_mysql',
                    'host'        => "127.0.0.1",
                    'dbname'      => "crud",
                    'username'    => "USER",
                    'password'    => "PASS",
                    'port'        => "3306",
                    'sshkey'      => []
                ],                            
            ],
            
        ];
        
        if(isset($type) && isset($driver)){
            return $c[$type][$driver]; 
        }
        if(isset($type)){
            return $c[$type]['default'];
        }else{
            throw new \Exception('Type: '.$type.' Not Found');
        }
    }
}