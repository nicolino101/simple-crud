<!DOCTYPE html>
<html lang="en">
    <head>
	<?php require_once LAYOUT_PATH . '/head.php'?>
	</head>
    <body>
        <header class="header">
    	<?php require_once LAYOUT_PATH . '/navigation.php'?>
    	</header>
    	
    	<?=\application\service\Flash::display();?>
    	
    	<div id="content">
    	<?php require_once APPLICATION_PATH . $this->viewpath . '.php'?>
    	</div>
    	
        <div id="footer">
        <?php require_once LAYOUT_PATH . '/footer.php'?>
        </div>
    	
    	<?php require_once LAYOUT_PATH . '/scripts.php'?>
    </body>
</html>