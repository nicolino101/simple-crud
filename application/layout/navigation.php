<h1 class="logo"><a href="/"><?=\application\config\Config::get("company", 'info')['title']?></a></h1>
<button class="fa-solid fa-bars"></button>
<ul class="main-nav">
    <li><a href="/users">Users</a></li>
    <li><a href="/add/user">Add User</a></li>
    <li class="contact-btn"><a href="#">Contact</a></li>
</ul>
<div class="side-nav">
    <ul class="side-nav-inner">
        <li onmouseenter="this.classList.add('on');" onmouseleave="this.classList.remove('on');" class="link" link="/users">Users</li>
        <li class="link" link="/add/user">Add User</li>
        <li class="contact-btn side">Contact</li>
	</ul>
</div>
<div class="contact-container">
    <span class="bold-20">Call Nick: 702-***-8412</span><div class="close-btn" style="float:right;"><i class="fa-solid fa-close"></i></div>
    <form name="contact" id="contact_form" action="/contact" method="post">
    <input type="text" name="fullname" id="fullname" value="<?=isset($user) ? $user->getFirstName() : null?><?=isset($user) && $user->getLastName() != '' ? ' '.$user->getLastName() : null;?>" placeholder="Full Name" />
    <input type="text" name="user_mobile_number" id="user_mobile_number" value="<?=isset($user) ? $user->getMobileNumber() : null;?>" placeholder="Mobile Phone" />
    <input type="text" name="user_email" id="user_email" value="<?=isset($user) ? $user->getEmail() : null;?>" placeholder="Email" />
    <input type="text" name="subject" id="subject" value="" placeholder="Subject" />
    <textarea name="message" id="user_message" cols="35" rows="3" placeholder="Message"></textarea>
    <input type="submit" value="Submit" id="contact_form_submit_btn" class="submit-btn" />
    </form>
</div>