<?php namespace application\service;

use application\database\IModel;

/**
 * @desc Just a really stupid basic Request Object
 * @author nick
 *
 */
class Request
{
    private static $instance;
    private static $uri;
    private static bool $isPost = false;
    private static bool $isGet = false;
    private static bool $isFiles = false;
    private static bool $isCookie = false;  
    private static array $params = [];
    private static array|object $post = [];
    private static array $cookies = [];
    private static array $files = [];
    private static array $server = [];
     
    public static function getInstance()
    {
        return static::$instance ?? (static::$instance = new self());
    }
    
    public static function setServer()
    {
        static::$server = $_SERVER;
        static::setUri(static::getServer('REQUEST_URI'));
        
        switch(static::getServer('REQUEST_METHOD')){
            case 'POST':
                return static::setPost($_POST);
                break;
            case 'GET':
                return static::setParams($_GET);                
                break;
            case 'COOKIE':
                return static::setCookie($_COOKIE);
                break;
            case 'FILE':
                return static::setFile($_FILES);
                break;
        }       
    }
    
    public static function getServer(string $index = null)
    {
        if(!is_null($index)){
            return static::$server[$index];
        }
        return static::$server;
    }
    
    private static function setUri(string $uri) : bool
    {
        if(Sanitizer::sanitize($uri)){
            static::$uri = $uri;
            return true;
        }
        return false;
    }
    
    public static function getUri() : string
    {
        return static::$uri;
    }
    
    public static function setPost(array|object $post = []) : bool
    {
        if(!empty($post)){
            static::$isPost = true;
            if(($post = Sanitizer::sanitize($post))){
                static::$post = $post;
                return true;
            }
        }
        return false;
    }
    
    public static function setParams(array $params) : bool
    {
        static::$params = Sanitizer::sanitize($params) ?? [];
        return true;
    }
    /**
     * 
     * @param array $get - ['id' => 1]
     * @return bool
     */
    public static function setParam($index, $value) : bool
    {
        static::$isGet = true;
        
            if(($param = Sanitizer::sanitize([$index, $value]))){
                static::$params[$index] = $value;
                return true;
            }
        
        return true;
    }
    
    public static function getParams() : array
    {
        return static::$params ?? [];
    }
    
    public static function getParam(string|int $index) : mixed
    {
        return static::$params[$index] ?? null;
    }
    
    public static function setCookie(array $cookie = [])
    {
        static::$isCookie = true;
        if(($cookies = Sanitizer::sanitize($cookie))){
            static::$cookie = $cookie;
            return true;
        }   
        return false;
    }
    
    public static function setFiles(array $files)
    {
        static::$isFiles = true;
        if(($files = Sanitizer::sanitize($files))){
            static::$files = $files;
            return true;
        }
        return false;
    }
    
    public static function getPost(int|string|null $index = null) : array|null|string
    {
        if(!is_null($index)){
            return static::$post[$index];
        }
        return static::$post;
    }
       
    public static function isPost() : bool
    {
        return static::$isPost;
    }
    
    public static function isCookie() : bool
    {
        return static::$isCookie;
    }
    
    public static function isFiles() : bool
    {
        return static::$isFiles;
    }
    
    public static function isGet() : bool
    {
        return static::$isGet;
    }
    
    public static function setModel(IModel $model)
    {
        static::$model = $model;
    }
    
    public static function getModel() : IModel
    {
        return static::$model;
    }
}