<?php

namespace application\service;

use application\user\controller\UserController;
use application\user\controller\ContactController;

class Router
{
    public static $params = [];
    public static $post = [];

    public static function init()
    {   
        $request = Request::getInstance();
        
        if(session_status() == PHP_SESSION_NONE){
            session_start();
        }
        $matches = [];
        switch($uri = $_SERVER['REQUEST_URI']){
            case preg_match('~^/?$~', $uri) > 0:
            case preg_match('~^/users/?$~', $uri) > 0:
                $c = new UserController($request);
                $c->index();
                break;
            case preg_match('~^/add/user/?$~', $uri) > 0:                
                $c = new UserController($request);
                $c->addUser();              
                break;
            case preg_match('~^/create/user/?$~', $uri) > 0:
                if(!empty($_POST)){
                    $request->setPost($_POST);
                    $c = new UserController($request);
                    $c->createUser();
                }
                break;                        
            case preg_match('~^/edit/user/([0-9]+)?/?$~', $uri, $matches) > 0:
                if(!empty($matches)){
                    $request->setParam('id', (int)$matches[1]); 
                }
                $c = new UserController($request);
                $c->editUser();
                break;            
            case preg_match('~^/update/user/?$~', $uri) > 0:
                if(!empty($_POST)){
                    $request->setPost($_POST);
                    $c = new UserController($request);
                    $c->updateUser();
                }
                break; 
            case preg_match('~^/update/user/ajax/?$~', $uri) > 0: 
                $json = file_get_contents("php://input");
                $obj = json_decode($json);                  
                $request->setPost((array)$obj);                
                $c = new UserController($request);
                $c->updateUser();
                break; 
            case preg_match('~^/user/less/([0-9]+)/?$~', $uri, $matches) > 0:
                if(!empty($matches)){                   
                    $request->setPost(['id' => $matches[1]]);
                    $c = new UserController($request);
                    $c->deleteUser();
                }
                break; 
            case preg_match('~^/deactivate/user/?$~', $uri) > 0:
                if(!empty($_POST)){
                    $request->setPost($_POST);
                    $c = new UserController($request);
                    $c->deactivateUser();
                }
                break; 
            case preg_match('~^/reactivate/user/?$~', $uri) > 0:
                if(!empty($_POST)){
                    $request->setPost($_POST);
                    $c = new UserController($request);
                    $c->reactivateUser();
                }
                break;
            case preg_match('~^/contact/?$~', $uri) > 0:
                if(!empty($_POST)){
                    $request->setPost($_POST);
                    $c = new ContactController($request);
                    $c->index();
                }
                break; 
            default:
                Redirect::uri('/');
                break;
        }
        return false;
    }
}