<?php namespace application\service;

/**
 * @desc All Controllers should extend this...well if the controller is being used for MVC
 *       Could easily make this an API server.
 *       Of course, I would create a Response Object in that case
 *       
 *       @important: Router needs to inject the Request into this so all Controllers can access Requests
 * @author nick
 *
 */
abstract class AbstractController
{
    protected Request $request;
    protected View $view;
    protected array $params = [];
    
    public function __construct(Request $request)
    {            
        $this->request = $request;
        $this->params = $request->getParams();
        $this->setView();
        $this->init();
    }
    
    public function getRequest() : Request
    {
        if(isset($this->request) && $this->request instanceof Request){
            return $this->request;
        }else{
            return $this->request = new Request($_REQUEST);
        }
    }
    
    public function setView(string $view = null, array $vars = []) : View
    {
        return $this->view = new View($view, $vars);
    }
    
    public function getView() : View
    {
        if(isset($this->view) && $this->view instanceof View){
            return $this->view;
        }else{
            return $this->view = new View();
        }
    }
}