<?php namespace application\service;

class Error extends \Exception
{   
    public function __invoke(){
        $this->onApplicationShutdown();
    }
    public function onApplicationShutdown()
    {
        $error = error_get_last();
        
        if(!is_null($error)){
            $this->logException($error);
            var_dump($error); exit;                   
            Redirect::uri('/');           
        }
    }
    
    protected function logException($error)
    {
        $estring = json_encode($error);
        if(APPLICATION_ENV === 'testing'){
            file_put_contents('/var/log/nginx/slicktext.local.error.log', $estring."\n\r", FILE_APPEND);
        }
        if(APPLICATION_ENV === 'staging'){
            file_put_contents('/var/log/nginx/staging.slicktext.com.error.log', $estring."\n\r", FILE_APPEND);
        }
        if(APPLICATION_ENV === 'production'){
            file_put_contents('/var/log/nginx/slicktext.com.error.log', $estring."\n\r", FILE_APPEND);
        }
    }
}

