<?php namespace application\service; 

class Functions
{
    public static function camelCaseToSnake($str, $us = "_") {
        return strtolower(preg_replace('/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', $us, $str));
    }
    public static function snakeToCamelCase($str){
        return str_replace('_', '', ucwords($str, "_"));
    }
    public static function formatPhoneForOutput($phone)
    {
        if(strlen($phone) < 1){
            return $phone;
        }
        $input = ltrim($phone, '1');
        $output = '('.substr($input, -10, -7).') ' . substr($input, -7, -4) . "-" . substr($input, -4);
        return $output;
    }
}