<?php namespace application\service;

class Flash
{
    public static $instance;
    
    public static function getInstance()
    {
        if(isset(static::$instance)){
            return static::$instance;
        }
        return static::$instance = new self();
    }
    
    public static function add(string $message, string $mType = 'info')
    {
        if(!isset($_SESSION['flash'])){
            $_SESSION['flash'] = [];
        }
        array_push($_SESSION['flash'], ['message' => $message, 'type' => $mType]); 
    }
    
    public static function redirect(string $message = null, string $mType = 'success', string $uri = null)
    {
        static::redirector($message, $uri, $mType);
    }
    
    public static function redirector(string $message, string $uri, string $mType = 'success')
    {
        static::add($message, $mType);
        Redirect::uri($uri);
    }
    
    public static function display()
    {
        if(!isset($_SESSION['flash'])){
            return null;
        }
        $messages =  count($_SESSION['flash']) > 0 ? $_SESSION['flash'] : null;       
        if(!is_null($messages)){
            $output = null;
            foreach($messages as $val){
                
                if($val['type'] == 'error' || $val['type'] == 'danger'){
                    $class = 'danger';
                }
                if($val['type'] == 'success'){
                    $class = 'success';
                }
                if($val['type'] == 'warning'){
                    $class = 'warning';
                }
                if($val['type'] == 'info'){
                    $class = 'info';
                }
                if($val['type'] == 'primary'){
                    $class = 'primary';
                }
                $output .= '<div class="div-alert active '.$class.'">'.$val['message'].'</div>'."\n\r";
            }            

            if(!empty($_SESSION['flash'])){
                unset($_SESSION['flash']);
            }            
        }
        return $output;
    }
}