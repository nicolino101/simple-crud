<?php namespace application\service;

class Sanitizer
{
    protected static $qstringToArray = false;
    
    public static function sanitize($value)
    {
        $params = [];
        if(is_array($value)){
        
        
        foreach($value as $key=>$val){  
            $key = self::cleanParam($key);
            $val = self::cleanParam($val);
            $params[$key] = $val;                           
        }
        return $params;
        }else{
           return $params[0] = self::cleanParam($value);
        }
    }
    
    public static function changeQueryStringToArray(bool $bool = false)
    {
        if($bool === false){
            self::$qstringToArray = $bool;
        }
    }
    
    protected static function array($value)
    {   
        $params = [];
        
        foreach($value as $key=>$val){
            $key = self::cleanParam($key);
            $val = self::cleanParam($val);
            $params[$key] = $val;
        } 
        return $params;
    }
    
    protected static function object($obj)
    {
        $arr = get_object_vars($obj);
                
        foreach($arr as $key=>$val){             
            unset($obj->{$key});
            $key = self::cleanParam($key);
            $obj->{$key} = self::cleanParam($val);                          
        }
        return $obj;      
    }
    
    public static function cleanParam($value)
    {
        /* if(self::is_querystring($value)){
            $value = self::queryStringToArray($value);
            if(self::$qstringToArray === false){
                $value = self::buildQueryString($value);
            }
        } */
        if(is_scalar($value)){           
            $value = preg_replace('/<.+>|<.+>/', '',$value);
        }
        if(is_object($value)){
            $value = self::object($value);            
        }
        if(is_array($value)){
            $value = self::array($value);
        }       
        return $value;       
    }
    
    protected static function is_querystring($value){
        $matches = []; $p = '.+';
       
        if(is_string($value) && preg_match('~('.$p.')=('.$p.')&|('.$p.')%3('.$p.')%26~', $value, $matches)){
            return true;
        }
        return false;
    }
    
    protected static function queryStringToArray($str)
    {
        $return = null;
        
        if(strpos($str, 'http') == false){
            $str = 'http://sample.com/?'.$str;
        }
        $parts = parse_url($str);
        parse_str($parts['query'], $return);
        $nReturn = null;
        foreach($return as $key=>$val){
            $key = Sanitizer::cleanParam($key);
            $val = Sanitizer::cleanParam($val);
            $nReturn[$key] = $val;
        }
        
        return $nReturn;
    }
    
    public static function buildQueryString(array $data){
        $str = '';
        foreach($data as $key=>$val){
            $str .= $key.'='.urlencode($val).'&';
        }
        return rtrim($str,'&');
    }
}
