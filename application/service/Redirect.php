<?php namespace application\service;

class Redirect
{
    public static $instance;
    
    public static function getInstance()
    {
        if(isset(self::$instance)){
            return self::$instance;
        }
        return self::$instance = new self();
    }
    
    public static function url($uri){
        self::uri($uri);
    }
    
    public static function uri($uri){
        if(!headers_sent()){
            header('Location: '.$uri); exit;
        }
    }
}