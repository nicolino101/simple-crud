<?php namespace application\service;

class Logger
{
    private static $pdo;
    private static $file = ERROR_LOG; 
    private static $count = 0;
    private static $instance;
    
    public static function getInstance()
    {
        return static::$instance = new self();
    }
    
    public static function setLogFile($file)
    {
        static::$file = $file;
    }
    
    public static function addDebug($message)
    {
        if(!is_string($message)){
            $msg = json_encode($message);
        }else{
            $msg = $message;
        }
        
        file_put_contents(static::$file, date('Y-m-d H:i:s').': ~~~ '.$msg." ~~~\n", FILE_APPEND);
    }
    
    public static function log($message)
    {
        static::addDebug($message);
    }
}