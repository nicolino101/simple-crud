<?php namespace application\service;

class View
{
    protected $layout = 'default';
    protected $vars = [];   
    protected $viewpath;    
    protected $content;
      
    
    public function __construct(string $viewpath = null, array $vars = [], string $layout = 'default')
    {             
        $this->setVars($vars);
        $this->viewpath = $viewpath;
        $this->layout = $layout;  
    }
       
    protected function setVars(array $vars) : View
    {
        $vars = Sanitizer::sanitize($vars);
        
        foreach($vars as $key=>$val){
            $this->vars[$key] = $val;
        }
        return $this;
    }
    
    public function getVars() : array
    {       
        return $this->vars;                
    }
    
    public function setVar(string $index, mixed $value) : void
    {   
        if(array_key_exists($index, array_keys($this->vars))){
            $this->vars[$index] = Sanitizer::sanitize($value);
        }
    }
    
    public function getVar(string $index) : array
    {      
        if(array_key_exists($index, array_keys($this->vars))){
            return $this->vars[$index];
        }
        return [];
    }
    
    public function setContent($cpage)
    {
        $this->content = $cpage;
        return $this;
    }
    
    public function getContent()
    {
        return $this->content;
    }
    
    public function setLayout(string $layout = 'default')
    {
        $this->layout = $layout;
        return $this;
    }
    
    public function getLayout()
    {
        return $this->layout;
    }
  
    public function setViewPath(string $viewpath)
    {
        $this->viewpath = $viewpath;
        
        return $this;
    }
    
    public function getViewPath()
    {
        return $this->viewpath;
    }
    
    public function escape(mixed $data) : mixed
    {        
        return Sanitizer::sanitize($data);
    } 
    
    public function render($viewpath = null)
    {   
        if(!is_null($viewpath)){
            $this->viewpath = $viewpath;
        }
        foreach($this->vars as $key=>$val){           
            $$key = $this->escape($val);
        }       
        
        require_once LAYOUT_PATH . '/' . $this->layout.'.php';  
    }
    
    public function __get($prop){
        if($prop === 'post'){
            return;
        }
        if(is_callable($this->{$prop})){
            return $this->{$prop};  
        }
    }
    
    public function __set($prop, $val){  
        $this->{$prop} = $val;  
    }
          
    public function __call($method, array $args = [])
    {
        $matches = [];
        if(preg_match('/(set|get)([A-Z]{1}.+)/', $method, $matches) > 0){
            $action = $matches[1];           
            $property = Functions::camelCaseToSnake($matches[2]);            
            if($action == 'get'){
                return $this->{$property};
            }else{   
                $this->{$property} = $args[0] ?? null;
                return $this;
            } 
        }
    } 
}