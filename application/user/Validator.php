<?php namespace application\user;

use application\database\IModel;

/**
 * @desc Validates all incoming data - if valid assigns valid value to the user object
 *                                     else adds to Validator::$errors array
 * @author nick
 * Validator::validate(IModel $user, array $post) works like the constructor
 * @returns void
 * Validator::hasErrors() bool and Validator::getErrors() are all that's needed
 */
class Validator
{
    private static IModel $user;    
    private static array $errors = [];
    private static array $post = [];
    
    public static function validate(IModel $user, array $post) : void
    {
        static::$user = $user;
        static::$post = $post;
        
        foreach($post as $key=>$val){           
            if(method_exists(new static(), $key)){
                $result = static::{$key}(trim($val));
                if(false !== $result){
                    //$method = 'set'.$user->snakeToCamelCase($key);
                    if(!is_null($result)){
                        //var_dump(static::$user->offsetGet($key)); exit;
                        static::$user->offsetSet($key, trim($result));
                    }
                }
            }            
        }        
    }  
    
    public static function hasErrors() : bool
    {
        if(count(static::$errors) > 0){
            return true;
        }
        return false;
    }
    
    public static function getErrors() : array
    {
        return static::$errors;            
    }
    
    public static function id(mixed $id) : int|bool 
    {
        return (is_int($id) || is_null($id)) ? $id : false;
    }
    
    public static function first_name(mixed $input) : bool|string
    {
        if(preg_match('/^[a-zA-Z\s\-]{2,20}$/', $input) > 0){
            return $input;
        }else{
            array_push(static::$errors, ['first_name' => 'First Name must be between 2 and 20 letters,space, or hyphen']);
            return false;
        }
    }
    
    public static function last_name(mixed $input)
    {
        if(preg_match('/^[a-zA-Z\s\-]{2,20}$/', $input)){           
            return $input;
        }else{
            array_push(static::$errors, ['last_name' => 'Last Name must be between 2 and 20 letters,space, or hyphen']);
            return false;
        }
    }
    
    public static function email(mixed $input, $checkdns = false) : bool|string
    {
        if(preg_match("/^[\w\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$/",$input) > 0){
            if($checkdns === false){
                return $input;
            }
            $domain = substr($input, strpos($input, '@') + 1);
            if(true === checkdnsrr($domain)){                
                return $input;
            }
        }
        
        return false;
    }
    
    public static function formatPhone($phone, $inorout = 'output')
    {
        if(!isset($phone) || strlen($phone) < 10){
            return $phone;
        }
        $phone = preg_replace('/\D/', '', $phone);
        if(strlen($phone) == 11 && strpos($phone, '1') == 0){
            $phone = ltrim($phone,'1');
        }
        if($inorout == 'output'){
            if(strlen($phone) == 10){
                return preg_replace("/(\d{3})(\d{3})(\d{4})/", "$1-$2-$3", $phone);
            }
        }
        return $phone;
    }
    
    public static function mobile_number(mixed $input) : bool|string
    {   
        $input = static::formatPhone($input, 'input');
        if(strlen($input) === 10){
            //var_dump($input); exit;
            return $input;
        }
        array_push(static::$errors, ['mobile_number' => 'Mobile Number is invalid']);
        return false;
    }
    
    public static function zip(mixed $input) : bool|int
    {          
        $formatted = (int)preg_replace('/^\D{5}$/', "", $input);
        if(preg_match('/^[0-9]{5}$/', $formatted) >= 0){
            return $formatted;
        }
        array_push(static::$errors, ['zip' => 'Zip codes should only be 5 digits.']);
        return false;
    }
    
    public static function address1(mixed $input) : bool|string
    {
        if(preg_match('/^[0-9a-zA-Z\s\-:,\.#]{5,150}$/', $input)){
            return $input;
        }else{
            array_push(static::$errors, ['address1' => 'Street Address 1 must be between 5 and 150 letters, numbers, period, hashtag, space, comma, colon or hyphen']);
            return false;
        }
    }
    
    public static function address2(mixed $input) : bool|string|null
    {
        if(is_null($input) || $input == '' || preg_match('/^[0-9a-zA-Z\s\-:,\.#]{2,50}$/', $input)){
            return trim($input,'\s');
        }else{
            array_push(static::$errors, ['address2' => 'Street Address 2 must be between 2 and 50 letters, numbers, period, hashtag, space, comma, colon or hyphen']);
            return false;
        }
    }
    
    public static function city(mixed $input) : bool|string
    {
        if(preg_match('/^[a-zA-Z\s\-]{4,20}$/', $input)){
            return $input;
        }else{
            array_push(static::$errors, ['city' => 'City must be between 4 and 20 letters,space, or hyphen']);
            return false;
        }
    }
    
    public static function state(mixed $input) : bool|string
    {
        if(preg_match('/^[A-Z]{2}$/', $input)){
            return $input;
        }else{
            array_push(static::$errors, ['state' => 'State must be 2 uppercase letters only']);
            return false;
        }
    }
    
    public static function country(mixed $input) : bool|string
    {
        if(preg_match('/^[A-Z]{2}$/', $input)){
            return $input;
        }else{
            array_push(static::$errors, ['country' => 'Country must be 2 uppercase letters only']);
            return false;
        }
    }
    
    public static function lat(mixed $input) : bool|string|null
    {           
        if(is_null($input)){
            return $input;
        }elseif(!is_null($input)){
            $r = preg_replace('/^\((.*)\)/', "$1", $input); 
            return($r);
        }else{
            array_push(static::$errors, ['lat' => 'Latitude is invalid']);
            return false;
        }
    }
    
    public static function lon(mixed $input) : bool|string|null
    {
        if(is_null($input)){
            return $input;
        }elseif(!is_null($input)){
            $r = preg_replace('/^\((.*)\)/', "$1", $input);
            return($r);
        }else{
            array_push(static::$errors, ['lon' => 'longitude is invalid']);
            return false;
        }
    }
    
    public static function timezone(mixed $input) : bool|string
    {
        return $input;
    }   
}