<?php namespace application\user\controller;

use application\service\AbstractController;
use application\service\Request;
use application\service\View;
use application\database\IModel;
use application\user\model\User;

class ContactController extends AbstractController
{
    private IModel $model;
    
    public function init()
    {       
        $this->model = new User();       
    }
    /**
     * @desc Display all users for edit or delete
     *       Normally we would only give access to an Admin here.
     */ 
    public function index() : void
    {   
        if(Request::isPost()){
            $post = Request::getPost();
            $view = new View('/user/view/contact');
            $message = 'Thank you for your Submission.';
            $view->setVars(['post' => $post, 'message' => $message]);
            $view->render(); 
        }
    }    
}