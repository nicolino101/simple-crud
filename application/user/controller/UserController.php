<?php namespace application\user\controller;

use application\service\AbstractController;
use application\service\Request;
use application\service\View;
use application\database\IModel;
use application\user\model\User;
use application\user\Validator;
use application\service\Flash;
use application\service\Redirect;
use application\config\Config;

class UserController extends AbstractController
{
    private IModel $model;
    
    public function init()
    {
        $this->model = new User();        
    }
    /**
     * @desc Display all users for edit or delete
     *       Normally we would only give access to an Admin here.
     */ 
    public function index() : void
    {   
        $view = new View();
        // could use $this->model->getAll() but no ordering
        $users = $this->model->read('1 ORDER BY id DESC');
        if($users){
            $view->setVars(['title' => 'View Users', 'users' => $users]);
        }else{
            $view->setVars(['title' => 'View Users', 'users' => null]);
        }
        
        $view->render('/user/view/index');
    }    
    
    /**
     * Display Form with empty user info.
     * @param array $params
     */
    public function addUser(null|View $view = null) : void
    {        
        $user = $this->model->getEmptyUser();   
        if(!is_null($view)){
            $view->user = $user;
            $view->render('/user/view/form');
        }else{
            $view = new View('/user/view/form', ['title' => 'Add New Profile', 'formAction' => '/create/user', 'user' => $user]);
            $view->render();
        }
    }
    
    /**
     * @desc creat a new user
     */
    public function createUser() 
    {
        if(Request::isPost()){           
            $post = Request::getPost();           
            $user = new User();
            $user->validate($post); 
            //var_dump($user, Validator::hasErrors()); exit;
            if(Validator::hasErrors()){                
                $view = new View('/user/view/form');                
                $message = 'Sorry, something went wrong...Please Try Again.';               
                $view->setVars(['formAction' => '/create/user', 'post' => $post, 'user' => $user, Validator::getErrors(), 'message' => $message]);               
                $view->render(); 
            }else{
                $lid = $user->save();
                if(false != $lid){                
                    $message = 'Successfully Created User: '.$lid;
                    Flash::redirect($message, 'success', '/edit/user/'.$lid); exit;              
                }
            }
        }
        Redirect::uri('/add/user');
    }
    
    /**
     * Display Form with user info. for editing
     * 
     */
    public function editUser(null|View $view = null) : void
    {  
        $id = $this->getRequest()->getParam('id');
        if(!$id){
            Flash::redirect('You must add a Profile', 'warning', '/add/user');
        }
        $user = $this->model->findById($id); 
        if(!is_null($view)){
            $view->setTitle('Edit Profile');
            $view->setAction('/update/user');
            $view->setUser($user);
            $view->render();
        }else{
            $view = new View('/user/view/form', ['title' => 'Edit Profile', 'formAction' => '/update/user', 'user' => $user]);  
            $view->render();
        }
    }
    
    public function updateUser() : void
    {
        if(Request::isPost()){
            $post = Request::getPost();
            $id = Request::getPost('id');
            $user = $this->model->findById($id);            
            $user->validate($post);            
            if(Validator::hasErrors()){
                $view = new View('/user/view/form');
                $message = 'Sorry, something went wrong...Please Try Again.';
                $view->setVars(['formAction' => '/create/user', 'post' => $post, 'user' => $user, Validator::getErrors(), 'message' => $message]);
                $view->render(); 
            }else{
                if($user->save()){
                    $message = 'Successfully Updated User: '.$user->getId();
                    Flash::redirect($message, 'success', '/edit/user/'.$user->getId());
                }
            }
        }
        Redirect::uri('/users');
    }
   
    /**
     * or you can do $this->model->deleteUser($id);
     * @return bool
     */
    public function deleteUser() : bool
    {
        if(Request::isPost()){            
            $id = Request::getPost('id');
            $user = $this->model->findById($id);
            
            if($user){
                $user->delete();
            }
            Flash::redirect('Successfully Deleted User', 'success', '/users');
        }
        Flash::redirect('Oooops', 'danger', '/users');
    }
    
    // didn't test this as it wasn't part of the requirement
    public function deactivateUser() : bool
    {
        if(Request::isPost()){            
            $id = Request::getPost('id');
            $user = $this->model->findById($id);
            if($user){
                $user->setActive(0)->save();                
            }
        }
    }
    // didn't test this as it wasn't part of the requirement
    public function reactivateUser() : bool
    {
        if(Request::isPost()){           
            $id = Request::getPost('id');
            $user = $this->model->findById($id);
            if($user){
                $user->setActive(1)->save();                
            }
        }
    }
}