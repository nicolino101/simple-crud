<?php 
/**
 *  Build a basic form that allows for the creation, editing and deleting of user objects. 
 *  The form should be responsive, look awesome on mobile.
 */
?>
<div class="container">
    
    <div id="form_messages" class="form-messages"><?=$form_message ?? null?></div>
    <div id="title" class="title"><?=$title ?? null?></div>
    <div class="form-container">   
		
		<p class="note"><em><i class="fa-solid fa-asterisk green-asterisk"></i> = required field</em></p>
        
        <form class="main-form" id="user_profile_form" name="user_profile_form" action="<?=$formAction ?? '/update/user';?>" method="post" autocomplete="off">           
            
            <?php if(!empty($post) && isset($post['id'])){ $id_value = $post['id']; }else{ $id_value = $user->getId() ?? null; }?>
            <?php if(isset($id_value)):?>
            <input type="hidden" value="<?=$id_value?>" name="id" id="<?=$post['id'] ?? $user->getId();?>" />
            <?php endif;?>
            <?php if(!empty($post) && isset($post['lat'])){ $lat_value = $post['lat']; }else{ $lat_value = $user->getLat() ?? null; }?>
            
            <input type="hidden" value="<?=$lat_value ?? null;?>" id="lat" name="lat" />                   
            
            <?php if(!empty($post) && isset($post['lon'])){ $lon_value = $post['lon']; }else{ $lon_value = $user->getLon() ?? null; }?>
            
            <input type="hidden" value="<?=$lon_value ?? null;?>" id="lon" name="lon" />                   
            
            <?php if(!empty($post) && isset($post['timezone'])){ $timezone_value = $post['timezone']; }else{ $timezone_value = $user->getTimezone() ?? null; }?>
            
            <input type="hidden" value="<?=$timezone_value ?? null?>" id="timezone" name="timezone" required />               
            
            <div class="form-group">                                   
                
                <label class="form-label" for="first-name">First Name <i class="fa-solid fa-asterisk"></i></label>     
                <input class="form-control" value="<?=$post['first_name'] ?? $user->getFirstName();?>" name="first_name" type="text" id="first_name" placeholder="Enter your First Name" autocomplete="off"><i class="input-add-on fa-solid fa-user"></i>                                                  
                <div class="label-spacer">&nbsp;</div> 
                <div id="error_first_name" class="nerror <?=isset($errors) && $errors['first_name'] ? 'active' : null;?>">
                    <div class="e-inner"></div>                  
                </div>
                               
        		<label class="form-label" for="last-name">Last Name <i class="fa-solid fa-asterisk"></i></label>  
        		<input contenteditable="true" class="form-control" value="<?=$post['last_name'] ?? $user->getLastName();?>" name="last_name" type="text" id="last_name" placeholder="Enter your Last Name" autocomplete="off"><i class="input-add-on fa-solid fa-user"></i>     			
      			<div class="label-spacer">&nbsp;</div> 
      			<div id="error_last_name" class="nerror <?=isset($errors) && $errors['last_name'] ? 'active' : null;?>">
      			    <div class="e-inner"></div>
      			</div>
      			
        		<label class="form-label" for="email">Email <i class="fa-solid fa-asterisk"></i></label>  
        		<input class="form-control" value="<?=$post['email'] ?? $user->getEmail();?>" name="email" type="email" id="email" placeholder="Enter your Email"><i class="input-add-on fa-solid fa-envelope" autocomplete="off"></i>
      			<div class="label-spacer">&nbsp;</div> 
      			<div id="error_email" class="nerror <?=isset($errors) && $errors['email'] ? 'active' : null;?>">
      			    <div class="e-inner"></div>
      			</div>      			
      			
        		<label class="form-label" for="mobile_number">Mobile Number <i class="fa-solid fa-asterisk"></i></label>  
        		<input class="form-control" value="<?=$post['mobile_number'] ?? $user->getMobileNumber();?>" name="mobile_number" type="tel" id="mobile_number" placeholder="Enter your Mobile Phone #" autocomplete="off"><i class="input-add-on fa-solid fa-mobile-screen"></i>                 		
      		    <div class="label-spacer">&nbsp;</div> 
      		    <div id="error_mobile_number" class="nerror <?=isset($errors) && $errors['mobile_number'] ? 'active' : null;?>"">
      		        <div class="e-inner"></div>
      		    </div>
      		         		           	
          	</div>
          	
          	<div class="form-group"> 
          	        	            	                   				
                <label class="form-label" for="address1">Street Adress <i class="fa-solid fa-asterisk"></i></label>
                <input class="form-control" value="<?=$post['address1'] ?? $user->getAddress1();?>" placeholder="Your Street Address" id="address1" name="address1" required /><i class="input-add-on fa-solid fa-location-dot"></i>               
                <div class="label-spacer">&nbsp;</div> 
                <div id="error_address1" class="nerror <?=isset($errors) && $errors['address1'] ? 'active' : null;?>">
                	<div class="e-inner"></div>
                </div>
                
                <label class="form-label" for="address2">Apartment, Unit, Floor # or Suite</label>
                <input class="form-control" value="<?= $post['address2'] ?? $user->getAddress2();?>" placeholder="Apt., Unit, Floor #, Suite" id="address2" name="address2" /><i class="input-add-on fa-solid fa-building"></i>
                <div class="label-spacer">&nbsp;</div> 
                <div id="error_address2" class="nerror <?=isset($errors) && $errors['address2'] ? 'active' : null;?>">
                	<div class="e-inner"></div>
                </div>
                
                <label class="form-label" for="city">City <i class="fa-solid fa-asterisk"></i></label>
                <input class="form-control" value="<?=$post['city'] ?? $user->getCity();?>" placeholder="City" id="city" name="city" required /><i class="input-add-on fa-solid fa-city"></i>                    
                <div class="label-spacer">&nbsp;</div> 
                <div id="error_city" class="nerror <?=isset($errors) && $errors['city'] ? 'active' : null;?>">
                	<div class="e-inner"></div>
                </div>
                
                <label class="form-label" for="state">State/Province <i class="fa-solid fa-asterisk"></i></label>
                <input class="form-control" value="<?=$post['state'] ?? $user->getState();?>" placeholder="State ex. NV" id="state" name="state" required /><i class="input-add-on fa-solid fa-location-dot"></i>
                <div class="label-spacer">&nbsp;</div> 
                <div id="error_state" class="nerror <?=isset($errors) && $errors['state'] ? 'active' : null;?>">
                	<div class="e-inner"></div>
                </div>
                
                <label class="form-label" for="zip">Zip code <i class="fa-solid fa-asterisk"></i></label>
                <input class="form-control" value="<?=$post['zip'] ?? $user->getZip();?>" placeholder="5 Digit Zip Code" id="zip" name="zip" required /><i class="input-add-on fa-brands fa-usps"></i>      
                <div class="label-spacer">&nbsp;</div> 
                <div id="error_zip" class="nerror <?=isset($errors) && $errors['zip'] ? 'active' : null;?>">
                	<div class="e-inner"></div>
                </div>
                
                <label class="form-label" for="country">Country/Region <i class="fa-solid fa-asterisk"></i></label>
                <input class="form-control" value="<?=$post['country'] ?? $user->getCountry();?>" placeholder="Country ex. US" id="country" name="country" required /><i class="input-add-on fa-solid fa-flag-usa"></i>
                <div class="label-spacer">&nbsp;</div>                
                <div id="error_country" class="nerror <?=isset($errors) && $errors['country'] ? 'active' : null;?>">
                	<div class="e-inner"></div>
                </div>
                      
                <div class="form-row center"> 
                    <input type="submit" id="submitBtn" class="form-button info-btn" value="Save" />
                    <div class="gutter"></div>
                </div>
                                                 
            </div>
             
        </form>
                    
	</div>
	
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=<?=\application\config\Config::get('api','google')['api_key']?>&callback=initAutocomplete&libraries=places&v=weekly" defer></script>
<script src="/js/v@lidator.js"></script>

<script> const validator = new Validator(); </script>
 
<script>
const form = document.getElementById('user_profile_form');
<?php // This only does validateAll() if array $post is set in the controller -> view 
      // If Validator::hasErrors(); when page loads it revalidates; You know what I mean....?>
<?php if(isset($post)):?>
window.onload = () => {
    validateAll();
}
<?php endif;?>

if(form){
    formFunctions();
}

function formFunctions(){
    form.first_name.focus();
    const submitBtn = document.getElementById('submitBtn');
    submitBtn.onclick = function(e){
        e.preventDefault();
        if(validateAll()){            
            let errors = document.querySelectorAll('.nerror.active');
            if(errors.length < 1){                
                form.submit();                                    
            }else{
                console.log(errors);
            } 
        }  
    }
    
    validator.formatPhone(this);
    
    // add 3 evt listeners to each form field
    const evts = ['keypress','focusout', 'change'];
    
    evts.forEach(function(evt){        
        form.first_name.addEventListener(evt, function(e){            
            validator.onlyAllowChars(this, 'a-zA-Z\s#\.\-');        
        	validator.validString(this, 'First Name should be between 2 and 30 characters, upper and lower case letters are allowed', 2, 30); 
    	});
        form.last_name.addEventListener(evt, function(e){           
            validator.onlyAllowChars(this, 'a-zA-Z\s#\.\-');        
            validator.validString(this, 'Last Name should be between 2 and 30 characters, upper and lower case letters are allowed', 2, 30); 
    	});
        form.email.addEventListener(evt, function(){
        	validator.validEmail(this, 'Email is not valid', false);
    	});     
        
        form.address1.addEventListener(evt, function(){
            setTimeout(()=>{
        		validator.validStreet(this, 'Address 1 should be between 5 and 50 characters', 5, 50);
            },1000);
        });
        form.address2.addEventListener(evt, function(){
        	validator.validAddress2(this, 'Address 2 should be between 3 and 50 characters or can be empty', 3, 50);
        });
        form.city.addEventListener(evt, function(){
            setTimeout(()=>{
        	    validator.validString(this, 'City is invalid', 5, 30);
            },1000);
        }); 
        form.state.addEventListener(evt, function(){
            setTimeout(()=>{
                //validator.onlyAllowChars(this, 'A-Z'); 
                validator.validString(this, 'State must be 2 uppercase letters only', 2, 2);
            },1000);
        }); 
        form.country.addEventListener(evt, function(){
            setTimeout(()=>{
            	//validator.onlyAllowChars(this, 'A-Z'); 
            	validator.validString(this, 'Country must be 2 uppercase letters only', 2, 2);
            },1000);
        }); 
        form.zip.addEventListener(evt, function(){
            setTimeout(()=>{
        		validator.onlyAllowChars(this, '0-9( )-');
        		validator.validZipcode(this, 'Zipcode should be 5 digits only');
            },1000);
        }); 
    });
    // ValidateAll() is called window.onload and form.submit()
    function validateAll(){
        validator.validEmail(form.email, 'Email is invalid', false);    
              
        validator.validName(form.first_name, 'First Name should be between 2 and 30 characters and cannot be empty. Upper and lower case letters are allowed', 2, 30);                           
    
        validator.validName(form.last_name, 'Last Name should be between 2 and 30 characters and cannot be empty. Upper and lower case letters are allowed', 2, 30);               
               
        validator.validStreet(form.address1, 'Address 1 cannot be empty');

        validator.validCity(form.city, 'City cannot be empty');

        validator.validState(form.state, 'State cannot be empty');

        validator.validCountry(form.country, 'Country cannot be empty');
        
        validator.validPhone(form.mobile_number, 'Mobile Number cannot be empty', 10, 11);
        
        validator.validZipcode(form.zip, 'Zipcode should be 5 digits only and cannot be empty');  
        return true;     
    }  
}
</script>