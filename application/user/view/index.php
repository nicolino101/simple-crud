 <h1 class="tcenter">Users</h1>
 <div class="responsive-container container">           
    <div class="responsive-inner">
        <table class="table" id="users_table">
            <th class="id">Id</th>
            <th class="first_name">First Name</th>
            <th class="last_name">Last Name</th>
            <th class="email">Email</th>
            <th class="mobile_number">Mobile Number</th>
            <th class="address">Address</th>
            <th class="city">City</th>
            <th class="state">State</th>
            <th class="zip">Zip</th>
            <th class="country">Country</th>
            <th class="timezone">Timezone</th>
            <th class="lat">Latitude</th>
            <th class="lon">Longitude</th>
            <th class="active">Active</th>
            <th class="created">Created</th>
            <th class="last_updated">Last Updated</th>
            <th>Edit</th>
            <th>Delete</th>
            <?php if(!empty($users)):?>            
            <?php foreach($users as $user):?>
            <tr>
                <td class="id btn-info btn-xs" data-id="<?=$user->getId();?>">                  
                   <?=$user->getId()?>                   
                </td>
                <td class="first_name">                	
                	<?=$user->getFirstName()?>               	
                </td>
                <td class="last_name">                	
                	<?=$user->getLastName()?>
                </td>
                <td class="email">
                    <div class="inner">
                		<?=$user->getEmail()?>
                	</div>
                </td>
                <td class="phone">
                    <div class="inner">
                		<?=$user->getMobileNumber();?>
                	</div>
                </td>
                <td class="address">
                	<div class="inner">
                		<?=$user->getAddress1()?> <?=$user->getAddress2()?>
                	</div>
                </td>
                <td class="city">
                	<?=$user->getCity()?>
                </td>
                <td class="state">
                	<?=$user->getState()?>
                </td>
                <td class="zip">
                	<?=str_pad($user->getZip(),5,'0', STR_PAD_LEFT);?>
                </td>
                <td class="country">
                	<?=$user->getCountry()?>
                </td>
                <td class="timezone">
                	<?=$user->getTimezone()?>
                </td>               
                <td class="lat">
                    <div class="inner">
                		<?=strlen($user->getLat()) > 0 ? round($user->getLat(),4) : null;?>
                	</div>
                </td>
                <td class="lon">
                	<div class="inner">
                		<?=strlen($user->getLon()) > 0 ? round($user->getLon(),4) : null;?>
                	</div>
                </td>
                <td class="active">
                	<?=$user->getActive() == 1 ? 'Active' : 'Deactivated'?>
                </td>
                <td class="created">
                	<div class="inner">
                		<?=$user->getCreated()?>
                	</div>
                </td>
                <td class="last_updated">
                	<div class="inner">
                		<?=$user->getLastUpdated()?>
                	</div>
                </td>
                <td>
                	<button id="edit_btn_<?=$user->getId()?>" data-id="<?=$user->getId()?>" class="warning-btn">Edit</button>
                </td>
                <td>
                	<button id="del_btn_<?=$user->getId()?>" data-id="<?=$user->getId()?>" class="danger-btn">Delete</button-md>
                </td>
            </tr>
            <?php endforeach; ?>
            <?php else:?>
            <tr><td colspan="16"><h3>Sorry...No Results. <a class="submit-btn" href="/add/user">Add A New User</a></h3></td></tr>
            <?php endif;?>
        </table>
    </div>
</div>
        
<script>
const editBtns = document.querySelectorAll('.warning-btn');
const trs = document.querySelectorAll('.id');
trs.forEach(tbtn => {
	tbtn.onclick = (e) => {
		e.preventDefault();
		const uid = tbtn.getAttribute('data-id');
		location.href="/edit/user/"+uid;
	}
});
editBtns.forEach(ebtn => {
	ebtn.onclick = (e) => {
		e.preventDefault();
		const uid = ebtn.getAttribute('data-id');
		location.href="/edit/user/"+uid;
	}
});
const delBtns = document.querySelectorAll('.danger-btn');
delBtns.forEach(dbtn => {
	dbtn.onclick = (e) => {
		e.preventDefault();
		if(confirm('Are You Sure?') == true){			
			location.href = '/user/less/'+dbtn.getAttribute('data-id');			
		}else{
            //do nothing
		}
		const uid = dbtn.getAttribute('data-id');
		console.log(uid)
	}
});
</script>
   