<div class="container">
<h3 style="text-align:center;"><?=$message;?></h3>


<table style="max-width:96vw;margin:0 auto;height:auto;border:1px solid silver;border-radius:5px;">
    <?php foreach($post as $key=>$val): ?>
    <tr>
        <?php 
        $word='';
        $parts = explode('_',$key);
        foreach($parts as $w){
            $word .= ucfirst($w).' ';
        }
        ?>
        <td style="padding:0;text-align:right;border:0;"><?=$word?></td>
        <td style="padding:0px 5px;border:0;">&nbsp;</td>
        <td style="padding:0;border:0;"><?=ucwords($val);?></td>
    </tr>
    <?php endforeach;?>
</table>

</div>