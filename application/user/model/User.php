<?php namespace application\user\model;

use application\database\Db;
use application\database\IModel;
use application\database\AbstractModel;
use application\user\Validator;

/**
 * @desc   Simple User Model for CRUD applications - Assumes the use of PDO for database
 *         Build a class in PHP that manages “user” objects. 
 *         Each object represents a unique row in a MySQL table called “users.” 
 *         Each column in the table represents a property of the user object. 
 *         
 *         The class should support a handful of actions including basic database CRUD functionality. 
 *         Those actions are:
 *         Get all object properties from the model
 *         Get a single object property from the model
 *         Set object properties in bulk in the model
 *         Set a single object property in the model
 *         Validation of the object’s properties
 *         Fetch a row from the database that sets the model properties
 *         Create a row in the database from properties in the model
 *         Update a row in the database from properties in the model
 *         Delete a row in the database from the primary key in the model
 *
 * @todo   For maximum re-usablitity we should create a DBConnection Adapter 
 *         and move the sql statements and binds to the individual adapters
 *         for instance: SqlLite Adapter, MSSql Adapter, or Postgres Adapter 
 */
class User extends AbstractModel implements IModel, \ArrayAccess
{  
    protected          string $tablename       = 'users';
    protected          string $dbname          = 'crud';
    protected          string $primaryKey      = 'id';
    
    private              ?int $id              = null;          // primaryKey
    private           ?string $first_name      = null;
    private           ?string $last_name       = null;
    private           ?string $email= null;
    private           ?string $mobile_number   = null;   
    private           ?string $address1        = null;
    private           ?string $address2        = null;          // apt #, unit, suite, floor, etc...
    private           ?string $city            = null;
    private           ?string $state           = null;
    private              ?int $zip             = null;          // 5 digits
    private           ?string $country         = null;          // 2 Char country
    private           ?string $lat             = null;          // Users address Latitude
    private           ?string $lon             = null;          // Users address Longitude
    private           ?string $timezone        = null;          // -8 PST, -7 MST, -6 CST, -5 EST, etc...
    private              ?int $active          = 1;             // active=1, inactive=0 is default
    private  readonly ?string $created;                         // datetime created on insert
    private  readonly ?string $last_updated;                    // datetime created on null update
    
    public function __construct(array|null $userParams = [])
    {   
        // normally we would inject this with interface or constructor from the Router or a DIC
        $this->db = Db::getDb('crud'); // singleton
        
        if(!empty($userParams)){ $this->hydrate($userParams); }
    }
    
    // It's more clear what this does when I remove from the constructor
    private function hydrate(array $userParams) : void
    {       
        foreach($userParams as $property=>$value){
            if(property_exists($this, $property)){
                $this->{$property} = $value;
            }
        }        
    }
    
    /**
     * @desc This is fast code - not code fast - thinking about changing the name from create() to persist()
     *       This writes the object to the db
     *       Not the best place to add this but...
     *       I Should abstract this class to make more extensible ... maybe a PDO_MySqlAdapter::class or UserRepository???
     * @return int|bool
     */
    public function create() : int|bool
    {
        $sql = 'INSERT INTO '.$this->dbname.'.'.$this->tablename.' (first_name, last_name, email, mobile_number, address1, address2, city, state, zip, country, timezone, lat, lon)
                VALUES(:first_name, :last_name, :email, :mobile_number, :address1, :address2, :city, :state, :zip, :country, :timezone, :lat, :lon)';
        
        $stmt = $this->db->prepare($sql);
        
        if($stmt){
            $stmt->bindValue(':first_name', $this->getFirstName(), \PDO::PARAM_STR);
            $stmt->bindValue(':last_name', $this->getLastName(), \PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->getEmail(), \PDO::PARAM_STR);
            $stmt->bindValue(':mobile_number', $this->getMobileNumber(), \PDO::PARAM_STR);
            $stmt->bindValue(':address1', $this->getAddress1(), \PDO::PARAM_STR);
            $stmt->bindValue(':address2', $this->getAddress2(), \PDO::PARAM_STR);
            $stmt->bindValue(':city', $this->getCity(), \PDO::PARAM_STR);
            $stmt->bindValue(':state', $this->getState(), \PDO::PARAM_STR);
            $stmt->bindValue(':zip', $this->getZip(), \PDO::PARAM_INT);
            $stmt->bindValue(':country', $this->getCountry(), \PDO::PARAM_STR);
            $stmt->bindValue(':timezone', $this->getTimezone(), \PDO::PARAM_STR);
            $stmt->bindValue(':lat', $this->getLat(), \PDO::PARAM_STR);
            $stmt->bindValue(':lon', $this->getLon(), \PDO::PARAM_STR);
            if($stmt->execute()){
                return $this->db->lastInsertId('id');
            }
        }
        return false;
    }
    
    public function update() : bool
    {
        $sql = 'UPDATE '.$this->dbname.'.'.$this->tablename.' SET first_name = :first_name, last_name = :last_name, email = :email, mobile_number = :mobile_number, address1 = :address1,
                address2 = :address2, city = :city, state = :state, zip = :zip, country = :country, timezone = :timezone, lat = :lat, lon = :lon, `active` = :active';
        $sql .= ' WHERE id = :id';
        
        $stmt = $this->db->prepare($sql);
        
        if($stmt){
            $stmt->bindValue(':id', $this->getId(), \PDO::PARAM_INT);
            $stmt->bindValue(':first_name', $this->getFirstName(), \PDO::PARAM_STR);
            $stmt->bindValue(':last_name', $this->getLastName(), \PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->getEmail(), \PDO::PARAM_STR);
            $stmt->bindValue(':mobile_number', $this->getMobileNumber(), \PDO::PARAM_STR);
            $stmt->bindValue(':address1', $this->getAddress1(), \PDO::PARAM_STR);
            $stmt->bindValue(':address2', $this->getAddress2(), \PDO::PARAM_STR);
            $stmt->bindValue(':city', $this->getCity(), \PDO::PARAM_STR);
            $stmt->bindValue(':state', $this->getState(), \PDO::PARAM_STR);
            $stmt->bindValue(':zip', $this->getZip(), \PDO::PARAM_INT);
            $stmt->bindValue(':country', $this->getCountry(), \PDO::PARAM_STR);
            $stmt->bindValue(':timezone', $this->getTimezone(), \PDO::PARAM_STR);
            $stmt->bindValue(':lat', $this->getLat(), \PDO::PARAM_STR);
            $stmt->bindValue(':lon', $this->getLon(), \PDO::PARAM_STR);
            $stmt->bindValue(':active', $this->getActive(), \PDO::PARAM_INT);
            return $stmt->execute();
        }
    }
    
    /**
     * @desc active record type save for updating the current object and creating a new object
     *       update or create depends on if the primaryKey is available it is an update otherwise create
     * @return boolean|\PDOStatement
     */
    public function save()
    {
        if(!is_null($this->{$this->primaryKey})){ // update
            return $this->update();
        }else{
            return $this->create();
        }
    }
    
    /**
     *
     * @return array of IModel objects
     */
    public function getAll() : array
    {
        return $this->read();
    }
    
    /**
     * @desc find user by primaryKey
     * @param int $id;
     * @return IModel | null
     */
    public function FindById(null|int $pk) : IModel
    {
        $user = $this->read($this->primaryKey.' = :'.$this->primaryKey, [$this->primaryKey => $pk], '*', true);
        if($user instanceof IModel){
            return $user;
        }
        return $this->getEmptyUser();
    }
    
    /**
     * @desc I use this when showing the form to create a new User()
     * @return IModel but empty
     */
    public function getEmptyUser() : IModel
    {
        $props = $this->getProperties(4);
        
        $uarr = [];
        foreach($props as $p){
            $uarr[$p] = null;
        }
        
        return new $this(userParams:$uarr);
    }
    
    public function offsetExists(mixed $offset) : bool
    {
        return property_exists($this, $offset);
    }
    
    public function offsetGet(mixed $offset) : mixed
    {
        if($this->offsetExists($offset)){
            return $this->{$offset};
        }
    }
    
    public function offsetSet(mixed $offset, mixed $value) : void
    {
        if($this->offsetExists($offset)){
            $this->offsetSet($offset, $value);
        }
    }
    
    public function offsetUnset(mixed $offset) : void
    {
        if($this->offsetExists($this->offset)){
            unset($this->{$offset});
        }
    }
    /**
     * @desc any User object can call $user->validate($post);
     *       then if(Validator::hasErrors()){ //do some shit here }else{ $user->save(); }
     *       $user->save() will update or create dynamically
     * @param array $post
     */
    public function validate(array $post) : void
    {
        Validator::validate($this, $post);
    }
    
    /**
     * I could have made this private but this way it can be used to fetch Users with more flexability. I use this in UserController::index()
     * @desc User::read(string|null $predicates = '1', array|null $binds = [], string|null $fields = '*', bool|null $singleObjectReturn = false)
     * @param string $predicates ex. 'first_name = :first_name AND last_name = :last_name ORDER BY id DESC LIMIT 10 OFFSET 10'
     *                                I know... don't worry it's safe from sql injection. Its protected by $this->prepExecStmt($sql, $binds)
     * @param array $binds ex. ['first_name' => $first_name, 'last_name' => $last_name]
     * @param string $fields ex. 'first_name, last_name, address, city, state, zip'
     * @return array|IModel an empty array, array of IModels or if $singleObjectReturn === true return a single IModel
     */
    public function read(string|null $predicates = '1', array|null $binds = [], string|null $fields = '*', bool|null $singleObjectReturn = false) : array|IModel
    {
        $users = [];
        
        $sql = $this->getReadQuery($fields, $predicates);
        
        $stmt = $this->prepExecStmt($sql, $binds);
        
        if($stmt instanceof \PDOStatement){
            try{
                while($row = $stmt->fetch(\PDO::FETCH_ASSOC)){
                    if($singleObjectReturn === true){
                        return new $this($row);
                    }else{
                        $users[] = new $this(userParams:$row);
                    }
                }
            }catch(\PDOException $e){
                echo json_encode($e); exit;
            }
        }else{
            echo json_encode($stmt); exit;
        }
        return $users;
    }
             
    /**
     * @desc delete with a user object
     * $user = new User();
     * $user = $this->model->getById($id);
     * $user->delete();
     * @return bool
     */
    public function delete() : bool
    {
        $sql = 'DELETE FROM '.$this->dbname.'.'.$this->tablename.' WHERE id = :id';
        $stmt = $this->db->prepare($sql);
        if($stmt){
            $stmt->bindValue(':id', $this->id, \PDO::PARAM_INT);
            return $stmt->execute();
        }
    }
    
    /**
     * deactivate a user with a User object;
     * $user->deactivate()
     * 
     * @return bool
     */
    public function deactivate() : bool
    {
        $this->setActive(0);
        
        return $this->save();        
    }
    
    /**
     * reactivate a user with a User object;
     * $user->reactivate();
     * @return bool
     */
    public function reactivate() : bool
    {
        $this->setActive(1);
        
        return $this->save();        
    }       
      
    
    /**
     * Below are
     * Accessors (getters) and Mutators (setters)
     */    
    public function getId() : string|null
    {
        return $this->id;
    }
    
    public function setFirstName(string $first_name) : IModel
    {
        $this->first_name = $first_name;
        return $this;
    }
    
    public function getFirstName() : string|null
    {
        return $this->first_name;
    }
    
    public function setLastName(string $last_name) : IModel
    {
        $this->last_name = $last_name;
        return $this;
    }
    
    public function getLastName() : string|null
    {
        return $this->last_name;
    }
    
    public function setEmail(string $email) : IModel
    {
        $this->email = $email;
        return $this;
    }
    
    public function getEmail() : string|null
    {
        return $this->email;
    }
    
    public function setMobileNumber(string $mobile_number) : IModel
    {
        $this->mobile_number = Validator::formatPhone($mobile_number, 'input');
        return $this;
    }
    
    public function getMobileNumber() : string|null
    {
        return Validator::formatPhone($this->mobile_number, 'output');
    }
    
    public function setAddress1(string $address1) : IModel
    {
        $this->address1 = $address1;
        return $this;
    }
    
    public function getAddress1() : string|null
    {
        return $this->address1;
    }
    
    public function setAddress2(string $address2) : IModel
    {
        $this->address2 = $address2;
        return $this;
    }
    
    public function getAddress2() : string|null
    {
        return $this->address2;
    }
    
    public function setCity(string $city) : IModel
    {
        $this->city = $city;
        return $this;
    }
    
    public function getCity() : string|null
    {
        return $this->city;
    }
    
    public function setState(string $state) : IModel
    {
        $this->state = $state;
        return $this;
    }
    
    public function getState() : string|null
    {
        return $this->state;
    }
    
    public function setZip(int $zip) : IModel
    {
        $this->zip = $zip;
        return $this;
    }
    
    public function getZip() : int|null
    {
        return $this->zip;
    }
    
    public function setCountry(string $country) : IModel
    {
        $this->country = $country;
        return $this;
    }
    
    public function getCountry() : string|null
    {
        return $this->country;
    }
    
    public function setLat(string|null $lat) : IModel
    {
        $this->lat = $lat;
        return $this;
    }
    
    public function getlat() : string|null
    {
        return $this->lat;
    }
    
    public function setLon(string|null $lon) : IModel
    {
        $this->lon = $lon;
        return $this;
    }
    
    public function getlon() : string|null
    {
        return $this->lon;
    }
    
    public function setTimezone(string $timezone) : IModel
    {
        $this->timezone = $timezone;
        return $this;
    }
    
    public function getTimezone() : string|null
    {
        return $this->timezone;
    }
    
    public function setActive(int|null $active = 1) : IModel
    {
        $this->active = $active;
        return $this;
    }
    
    public function getActive() : string|null
    {
        return $this->active;
    }
    
    public function setCreated(string $created) : IModel
    {
        $this->created = $created;
        return $this;
    }
    
    public function getCreated() : string|null
    {
        return $this->created;
    }
    
    public function setLastUpdated(string $last_updated) : IModel
    {
        $this->last_updated = $last_updated;
        return $this;
    }
    
    public function getLastUpdated() : string|null
    {
        return $this->last_updated;
    }
}