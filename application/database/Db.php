<?php namespace application\database;

use application\config\Config;

class Db
{
    protected static $conn;
    public static $dbname = 'default';
    
    protected static function getConnection()
    {      
        try{
            $config = Config::get('db', static::$dbname);            
            static::$dbname = $config['dbname'];
            static::$conn = new \PDO("mysql:host=" . $config['host'] . ";dbname=" . $config['dbname'], $config['username'], $config['password']);
            static::$conn->setAttribute( \PDO::ATTR_EMULATE_PREPARES, false);           
        }catch(\PDOException $exception){
            echo "Connection error: " .static::$dbname.' - '. $exception->getMessage(); exit;
        }
        
        return static::$conn;
    }
    
    /** 
     * @desc sometimes we don't want to re-use the same connection - use this
     * @param string $dbname
     * @return \PDO
     */
    public static function getDbConnection(string $dbname) : \PDO
    {
        static::$dbname = $dbname;
        return static::getConnection();
    }
    
    /**
     * @desc Singleton of sorts - will reuse the same db connection if exists
     * @param string $dbname
     * @return \PDO
     */
    public static function getDb(string $dbname) : \PDO
    {      
        return (static::$conn instanceOf \PDO) ? static::$conn : static::$conn = static::getDbConnection($dbname);
    }
}