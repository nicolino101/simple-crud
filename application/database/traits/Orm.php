<?php namespace application\database\traits;

/** 
 * Not using this today
 * @author nick
 *
 */
trait Orm
{
    /* 
     * Decided not to use functions below
     */
    public function getColumnNames($dbname = null, $tablename = null)
    {
        $dbname = $dbname ?? $this->dbname;
        $tablename = $tablename ?? $this->tablename;    
        $sql = 'SELECT COLUMN_NAME FROM `information_schema`.`COLUMNS` WHERE TABLE_SCHEMA="'.$dbname.'" AND TABLE_NAME="'.$tablename.'";';
        $stmt = $this->db->query($sql);
        
        while($row = $stmt->fetchObject()){
            $cnames []= $row->COLUMN_NAME;
        }
        return $cnames;
    }
    
    public function getValidData(array $data) : array
    {
        $cnames = $this->getColumnNames();
        
        $validData = [];
        foreach($data as $key=>$val){
            if(in_array($key, $cnames)){
                if(!is_null($val)){
                    $validData[$key] = $val;
                }
            }
        }
        if(!empty($validData)){
            return $validData;
        }else{
            return [];
        }
    }
    
    public function __set($property, $value)
    {
        $this->{$property} = $value;
    }
    
    public function __get($property)
    {
        return $this->{$property};
    }
    
    public function __call($method, array $args = [])
    {
        if($method == 'query'){
            return;
        }
        $value = NULL;
        
        if (!empty($args)){
            $value = $args[0];
        }
        
        if (preg_match('/^set/', $method)) {
            $prop     = preg_replace("/set/", '', $method);
            $property = $this->camelCaseToSnake($prop);
            
            $this->{$property} = $value;
            
            return $this;
        } elseif (preg_match('/^get/', $method)) {
            $prop     = preg_replace("/get/", '', $method);
            $property = $this->camelCaseToSnake($prop);
            
            return $this->{$property};
        } else {
            throw new \Exception("Invalid Method: $method trapped in __call()");
        }
    }
    
    public function getPrimaryKeyIfExists()
    {
        $sql = 'SELECT COLUMN_NAME pk FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_NAME="PRIMARY" AND TABLE_NAME="' . $this->tablename . '" AND TABLE_SCHEMA="' . $this->dbname . '"';
        $stmt = $this->db->query($sql);
        return $stmt->fetchObject()->pk ?? 'id';
    }
    
    public static function camelCaseToSnake($str, $us = "_") {
        return strtolower(preg_replace('/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', $us, $str));
    }
    
    public static function snakeToCamelCase($str){
        return str_replace('_', '', ucwords($str, "_"));
    }
}