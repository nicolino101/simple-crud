<?php namespace application\database; 

abstract class AbstractModel extends \stdClass implements IModel
{
    protected $db;
     
    public function getCreateQuery(array $data) : array
    {
        $sql = 'INSERT INTO ' . $this->dbname . '.' . $this->tablename . ' ( ';
        
        $fields = '';
        $values = '';
        $binds = [];
        foreach ($data as $key => $val) {
            if($key != $this->primaryKey){
                $fields .= "`$key`, ";
                $values .= ":$key, ";
                $binds[$key] = $val;
            }
        }
        $sql .= rtrim($fields, ', ') . ') VALUES(' . rtrim($values, ', ') . ');';
        
        return [
            'sql' => $sql,
            'binds' => $binds
        ];
    }
    
    public function getReadQuery(string $fields = '*', string $predicates = '1') : string
    {
        $sql = 'SELECT '.$fields.' FROM '.$this->dbname.'.'.$this->tablename;
        return $sql .= ' WHERE '.$predicates;
    }
    
    public function getUpdateQuery() : string
    {
        $userArray = $this->toArray();
        $keyvals = '';
        $sql = 'UPDATE '.$this->dbname.'.'.$this->tablename.' SET ';
        foreach(array_keys($userArray) as $key){
            if(property_exists($this, $key) && $key != $this->primaryKey){
                $keyvals .= "$key = :$key,";
            }
        }
        $sql .= rtrim($keyvals,',');
        return $sql .= ' WHERE '.$this->primaryKey.' = :'.$this->primaryKey;
    }
    
    public function getDeleteQuery() : string
    {
        $sql = 'DELETE FROM '.$this->dbname.'.'.$this->tablename;
        return $sql .= ' WHERE '.$this->primaryKey.' = :'.$this->primaryKey;
    }
    
    public function getDeactivateQuery() : string
    {
        $sql = 'UPDATE '.$this->dbname.'.'.$this->tablename;
        $sql .= 'SET `active` = 0';
        return $sql .= ' WHERE '.$this->primaryKey.' = :'.$this->primaryKey;
    }
    
    public function getReactivateQuery() : string
    {
        $sql = 'UPDATE '.$this->dbname.'.'.$this->tablename;
        $sql .= 'SET `active` = 1';
        return $sql .= ' WHERE '.$this->primaryKey.' = :'.$this->primaryKey;
    }
    
    /**
     * @desc Just prepares sql security against sql injection executes and binds params
     * @param string $sql
     * @param array $binds
     * @return bool|\PDOStatement $stmt on succes false on fail
     */
    protected function prepExecStmt(string $sql, array|null $binds = []) : bool|\PDOStatement
    {
        $stmt = $this->db->prepare($sql);
        if($stmt){
            foreach($binds as $key=>$val){
                $stmt->bindParam($key, $val);
            }
            try{
                $stmt->execute();
                return $stmt;
            }catch(\PDOException $e){
                var_export($e->getMessage()); exit;
            }
        }
        return false;
    } 
    
    /**
     * deactivate a user with pk $model->deactivateUser($id);
     * @param int $pk
     * @return bool
     */
    public function deactivateUser(int $pk) : bool
    {
        $sql = $this->getDeactivateQuery();
        
        return $this->prepExecStmt($sql, [$this->primaryKey => $pk]);
    }
    
    /**
     * reactivate a user with pk $model->deactivateUser($id);
     * @param int $pk
     * @return bool
     */
    public function reactivateUser(int $pk) : bool
    {
        $sql = $this->getReactivateQuery();
        
        return $this->prepExecStmt($sql, [$this->primaryKey => $pk]);
    }
    
    /**
     * 
     * @param int $reflectionProperty 1 = public, 2 = protected, 4 = private, 128 = readonly
     * @return array
     */
    public function getProperties(int $reflectionProperty = 4)
    {       
        $props = [];
        $reflector = new \ReflectionClass($this);       
        foreach ($reflector->getProperties($reflectionProperty) as $o) {
            if(!$o->isReadOnly()){
                $props[] = $o->name;
            }
        }
        return $props;
    }
    
    public function propertyIsReadOnly(string $property) : bool
    {
        $reflector = new \ReflectionClass($this); 
        return $reflector->getProperty($property)->isReadOnly();
    }
    
    public function query($sql, $binds = []) : \PDOStatement
    {
        $stmt = $this->db->prepare($sql);
        try{
            if(!empty($binds)){
                $stmt->execute($binds);
            }else{
                $stmt->execute();
            }
        }catch(\PDOException $e){
            throw new \PDOException($e);
        }finally{
            return $stmt;
        }
        return $stmt;
    }
    
    public function getColumnNames()
    {
        $sql = 'SELECT COLUMN_NAME cname FROM information_schema.COLUMNS WHERE TABLE_SCHEMA="' . $this->dbname . '" AND TABLE_NAME="' . $this->tablename . '";';
        $stmt = $this->db->query($sql);
        $cnames = [];
        while($row = $stmt->fetchObject()){
            $cnames[] = $row->cname;
        }
        return $cnames;
    }
    
    public function getValidData(array $data) : array
    {
        $cnames = $this->getColumnNames();
        
        $validData = [];
        foreach($data as $key=>$val){
            if(in_array($key, $cnames)){
                if(!is_null($val)){
                    $validData[$key] = $val;
                }
            }
        }
        
        if(!empty($validData)){
            return $validData;
        }else{
            return [];
        }
    }
    
    public function getPrimaryKeyIfAvailable()
    {
        return $this->getPrimaryKeyIfExists();
    }
    
    public function getPrimaryKeyIfExists()
    {
        $sql = 'SELECT COLUMN_NAME pk FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE CONSTRAINT_NAME="PRIMARY" AND TABLE_NAME="' . $this->tablename . '" AND TABLE_SCHEMA="' . $this->dbname . '"';
        $stmt = $this->db->query($sql);
        return $stmt->fetchObject()->pk ?? 'id';
    }
    
    public function toArray() : array
    {
        $props = $this->getProperties();
        $objArray = [];
        foreach($props as $key){
            $method = 'get'.$this->snakeToCamelCase($key);
            $objArray[$key] = $this->{$method}();
        }
        return $objArray;
    }
    
    public static function camelCaseToSnake($str, $us = "_") {
        return strtolower(preg_replace('/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', $us, $str));
    }
    
    public static function snakeToCamelCase($str){
        return str_replace('_', '', ucwords($str, "_"));
    }
}