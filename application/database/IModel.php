<?php 

namespace application\database;

/**
 * @desc can be used for interface injection of db connection
 *       just for type grouping now on models
 * @author nick
 *
 */
interface IModel 
{
    //public function setDb(\PDO $db) : void;
    //public function getDb() : \PDO;
}