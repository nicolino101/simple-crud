CREATE TABLE `users` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`first_name` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`last_name` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`email` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`mobile_number` VARCHAR(32) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`address1` VARCHAR(355) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`address2` VARCHAR(355) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`city` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`state` VARCHAR(2) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`zip` INT(11) NULL DEFAULT NULL,
	`country` VARCHAR(2) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`timezone` VARCHAR(32) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`lat` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`lon` VARCHAR(128) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`active` TINYINT(1) NULL DEFAULT '1',
	`created` DATETIME NULL DEFAULT current_timestamp(),
	`last_updated` DATETIME NULL DEFAULT NULL ON UPDATE current_timestamp(),
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=21
;
