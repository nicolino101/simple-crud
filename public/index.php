<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/../application/autoload.php';
defined('APPLICATION_ENV') || define('APPLICATION_ENV', 'testing');
defined('APPLICATION_PATH') || define('APPLICATION_PATH', $_SERVER['DOCUMENT_ROOT'] . '/../application');
defined('LAYOUT_PATH') || define('LAYOUT_PATH', APPLICATION_PATH . '/layout');
use application\service\Router;
use application\service\Error;

register_shutdown_function([new Error(), 'onApplicationShutdown']);

Router::init();

?>