"use strict";

const bars = document.querySelector('.fa-bars');
const sidenav = document.querySelector('.side-nav');
const sidenavinner = document.querySelectorAll('.side-nav-inner');
const sidenavBtns = document.querySelectorAll('.side-nav-inner>.link');
const contactContainer = document.querySelector('.contact-container');
const contactBtns = document.querySelectorAll('.contact-btn');
const sideContactBtn = document.querySelector('.contact-btn.side');
const closeBtn = document.querySelector('.close-btn');

bars.onclick = (e) => {
    toggleSideNav();
}

sidenavBtns.forEach(function(sbtn) {
	sbtn.addEventListener('mouseenter',function(){
		sbtn.classList.add('on');
	});
	sbtn.addEventListener('mouseleave',function(){
		sbtn.classList.remove('on');
	});
	sbtn.addEventListener('click', function(){
		location.href = sbtn.getAttribute('link');
	});
});

sidenav.onclick = (e) => {	
    if(e.target != bars && e.target != sideContactBtn && e.target != contactContainer){
		closeSideNav();
    }
}

function closeSideNav(){
    if(sidenav.classList.contains('active')){
    	sidenav.classList.remove('active');
    }
}

function toggleSideNav(){
    if(sidenav.classList.contains('active')){
    	sidenav.classList.remove('active');
    }else{
        sidenav.classList.add('active');
    }
}

function uricheck(url){	
	sidenavBtns.forEach(function(btn) {
		let link = btn.getAttribute('link');
		let uri = url.pathname;		
		if(uri === link){
			btn.classList.add('active');
		}else{
			btn.classList.remove('active');
		}
	});
}

closeBtn.onclick = function(){
	this.parentElement.classList.remove('active');
}

contactBtns.forEach(cbtn => {
	cbtn.onclick = () => {
    	contactContainer.classList.toggle('active');
	}
});

window.onload = function(){
	uricheck(new URL(location.href));
}