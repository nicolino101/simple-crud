"use strict";

let autocomplete;

function initAutocomplete(){
	    form.address1.onkeyup = function() {	
			autocomplete = new google.maps.places.Autocomplete(this, {
	     		componentRestrictions: { country: ["us", "ca"] },
	     		fields: ["address_components", "geometry", 'formatted_phone_number', 'utc_offset_minutes'],
	     		types: ["address"]
			});
			FillInAddress(autocomplete);
		}
		form.zip.onkeyup = function() {	
			autocomplete = new google.maps.places.Autocomplete(this, {
	     		componentRestrictions: { country: ['us', 'ca'] },
	     		fields: ["address_components", "geometry", 'formatted_phone_number', 'utc_offset_minutes'],
	     		types: ["postal_code"]
			});
			FillInAddress(autocomplete);
		}
		form.city.onkeyup = function() {	
			autocomplete = new google.maps.places.Autocomplete(this, {
	     		componentRestrictions: { country: ['us', 'ca'] },
	     		fields: ["address_components", "geometry", 'formatted_phone_number', 'utc_offset_minutes'],
	     		types: ["locality"]
			});
			if(FillInAddress(autocomplete)){
				form.address1.focus();	
			}		
		}
		form.state.onkeyup = function() {	
			autocomplete = new google.maps.places.Autocomplete(this, {
	     		componentRestrictions: { country: ['us', 'ca'] },
	     		fields: ["address_components", "geometry", 'formatted_phone_number', 'utc_offset_minutes'],
	     		types: ["administrative_area_level_1"]
			});
			if(FillInAddress(autocomplete)){			
				form.city.focus();	
			}					
		}
}

function FillInAddress(autocomplete){
	autocomplete.addListener("place_changed", function() {
	    // Get the place details from the autocomplete object.
	    const place = this.getPlace();
	        
		const loc = place.geometry.location.toJSON();
		 
		form.lat.value = loc.lat;
	    form.lon.value = loc.lng;
	  
	    let utcOffset = Math.floor(place.utc_offset_minutes / 60);
	    form.timezone.value = utcOffset;
	  
	    for (const component of place.address_components) {    
	        const componentType = component.types[0];
	        //console.log(componentType)
	        switch (componentType) {
		        case "street_number": {
		            form.address1.value = component.short_name;
		            
		            break;
		        }
		        case "route": {
		            form.address1.value += ' '+component.short_name;
		            
		            break;
		        }
		        case "postal_code": {
		            form.zip.value = component.short_name;
		            
		            break;
		        }     
		        case "locality": {
		            form.city.value = component.short_name;
		            
		            break;
		        }
		        case "administrative_area_level_1": {
		            form.state.value = component.short_name;
		            
		            break;
		        }
		        case "country": {
		            form.country.value = component.short_name;
		            
		            break;
		        }
		        return true;
		    }
	    }	    
	});	
}
