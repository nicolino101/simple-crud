"use strict";
////////////////// v@lidator.js ///////////////////

class Validator {

	constructor() {
	    
    }
    	
    validEmail(el, msg, dns) {            
	    var value = el.value;       
		var regex   = /^[\w\.-]{1,}\@([\da-zA-Z-]{1,})\.{1}[\da-zA-Z-]{2,6}$/;
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
	    }else if(dns === true){
	        this.checkDns(el);
	    }else{
	        this.unsetError(el);
	        return true;
	    }			    							
	}

    validPhone(el, msg){
	    var regex   = /^[2-9]{1}[0-9]{2}\-[2-9]{1}[0-9]{2}\-[0-9]{4}$/;
		if(regex.test(el.value) === false){
		    this.setError(el, msg);
		    return false;
		}else{		
		    this.unsetError(el);
		    return true;
		}
	}

    validName(el, msg, min, max){
	    var value = el.value;       
		var regex = /^[a-zA-Z ]+$/;
		if(this.withinRange(value, min, max) === false){
			this.setError(el, msg);
		    return false;
		}else 
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}
	
	validCity(el, msg){
	    var value = el.value;       
		var regex = /^[a-zA-Z \-]{4,40}$/;		
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}
	
	validState(el, msg){
	    var value = el.value;       
		var regex = /^[A-Z]{2}$/;		
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}
	
	validCountry(el, msg){
	    var value = el.value;       
		var regex = /^[A-Z]{2}$/;		
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    withinRange(value, min, max){
		min = !min ? 1 : min;
		max = !max ? 900000000 : max;
		if(value.length < min || value.length > max){		
		    return false;
		}
		return true;
	}

    validLength(el, msg, min, max){
	    var value = el.value;
	    if(this.withinRange(value, min, max) === false){
			this.setError(el, msg);
		    return false;
		}
		return true;
	}	

    formatPhone(){
		const el = document.getElementById('mobile_number');		
		el.onchange = () => {
			return this.validPhone(el, 'Mobile Number is not valid');
		}	
	    let keypressCount = 0;
	    el.onkeypress = (e) => {
			e.preventDefault();
			keypressCount++;
			let key = e.key;
			let len = el.value.length;
			console.log(len, keypressCount) 
			if(len < 13){
				
				return this.phoneFormatter(el, len, keypressCount, key);			
			}
		}		        	    	    
	}

    phoneFormatter(el, len, kpc, key){
		if(len < 1){ 
	        if(key == 1){
	            this.setError(el, 'No leading 1 necessary');			            
	        }else{
			    el.value = el.value + key;
			    this.unsetError(el);
			}          
	    }else if(len <= 2){				
	        el.value = el.value + key; 		        				         
        }else if(len == 3){
			el.value = el.value + '-' + key;
		}else if(len <= 6){
			el.value = el.value + key;
		}else if(len == 7){
			el.value = el.value + '-' + key;
		}else if(len < 12){
			el.value = el.value + key;	
		}else if(len == 12){
			if(kpc == 1){
				el.value = '';
				this.phoneFormatter(el, 0, 1, key)
			}else{
				//el.value = el.value + key;
				if(this.validPhone(el)){
					return true;
				}
			}
		}	
	}

    phoneFormat(el, len){
		el.setAttribute('maxlength', 12)
		var nv = '';  		 
	    if(len == 3){
	       nv = '-';           
	    } else if(len == 7){
	       nv = '-';           
	    }	    
	    el.value = el.value+nv;
	    return true;
	}  

    validZipcode(el, msg){
	    var value = el.value;       
		var regex   = /^[0-9]{5}$/;		
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    setError(el, msg){	
		let errEl = document.getElementById('error_'+el.id);   
		errEl.classList.add('active');
		    let w = (msg.length*8)+'px';	
		errEl.innerHTML = '<div class="e-inner" style="min-width:'+w+'">'+msg+'</div>';		    		  
	}

    unsetError(el){	 
		let errEl = document.getElementById('error_'+el.id) ?? null;		
		if(errEl){ 
			errEl.classList.remove('active');               
	    }	    
	}

    canBeEmpty(el){
		var value = el.value;
		if (value.length < 1){
			this.unsetError(el);
			return true;
		}
	}

    validSentence(el, msg){
		var value = el.value;       
		var regex = /^[a-zA-Z0-9_\-\., ]+$/;
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
	    }else{
	        this.unsetError(el);
	        return true;
	    }
	} 
	
    notEmpty(el, msg){
		return el.value.length > 0;
	}

    checkDns(el){
	    var value = el.value;
	    /*var req = $.ajax({
		   type: "POST",
		   dataType: "json",             		   
		   url: "/ajax/dns",
		   data: { email:value }              		   
    	});*/
    	req.done(function(data){        	    
    	    if(data.response == false){
		        this.setError(el, 'Domain is not valid');
		        return false;
		    }else{
		        this.unsetError(el);
		        return true;
		    }
        });	
        req.fail(function(data){
            this.setError(el, msg);
        });	            
	}

    emailExists(el, msg){
	    var value = el.value;
	    var xhr = Utilities.ajax(
		    
		    '/users/search/dup-email', 
		    {email:value},
		    'json',
		    function(response) {	        		      		    
	    
	        obj = JSON.parse(response);
	        if(obj.valid === false){
	            this.setError(el, msg);
	            return false;
	        }else{
	            this.unsetError(el);
				return true;
	        }	         
	    });     
	}

    validWebsite(el, msg, dns) {            
	    var value = el.value;       
		var regex   = /^(https?):\/\/+(www\.)?[a-z0-9\-\.]{3,}\.[a-z]{2,6}$/;
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
	    }else{
	        this.unsetError(el);
	        return true;
	    }			    							
	}

    validUrl(el, msg, dns) {            
	    var value = el.value;       
		var regex   = /^(\b(https?|ftp|file):\/\/)?[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]$/;
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
	    }else{
	        this.unsetError(el);
	        return true;
	    }			    							
	}

    postcodePopulate(el){    
	    var value = el.value;
	    Utilities.jsonPost(
		    "/ajax/city-state",
		    { zipcode: value },              		   
    	    function(data){
    	    	obj = data;   	         		
		    	document.getElementById('city').value = obj.city;   			    
		    	document.getElementById('state').value = obj.state;
		   	 	document.getElementById('state').removeAttribute('disabled');
	        	document.getElementById('city').removeAttribute('disabled');           
				document.getElementById('country_id').value = obj.country_id;		
        	}).then(function(){            
				city.value = '';
				state.value = '';
				this.setError(el, 'Zipcode must be 5 digits');
        });		            
	}

    validPassword(el, msg, min, max){
	    var value = el.value;    
		var regex   = /^[0-9A-Za-z_!-$@#%]+$/;
		if(this.withinRange(value, min, max) === false){
			this.setError(el, msg);
		    return false;
		}else 
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

	/**
	*
	*	Password has to be at least one number -> (?=.*\d)
		and at least one  uppercase letter -> (?=.*[A-Z])
		and at least one  lowercase letter -> (?=.*[a-z])
	    one or more of these special characters: -> (?=.*[!@#$%])
		and there have to be 8-12 characters -> {8,12}
	*
	**/
	mustContainPassword(el){
	    var value = el.value;
	    var regex = /^(?=.{8,15}$)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\!@\#\$%\^&*\-_])/; 
	    if(regex.test(value) === false){
		    this.setError(el, 'Password must be between 8-15 characters, contain atleast 1 Number, 1 Lowercase Letter, 1 Uppercase Letter, and 1 of !@#$%_ ');
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

	validMatch(el1, el2, msg){
	    var value1 = el1.value;
	    var value2 = el2.value;
	    if(value1 === value2){
	         this.unsetError(el1);
	         return true;
	    }else{      
	         this.setError(el1, msg);
	         return false;
	    }    
	}

    validTaxNumber(el, msg){
	    var value = el.value;       
		var regex   = /^[0-9]{3}[-]?[0-9]{2}[-]?[0-9]{4}$/;
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    formatTaxNumber(el, msg){
		if(true === validTaxNumber(el, msg)){
	    	return true;
	    }
	    var nv = '';
	    let len = el.value.length;
	    let val = el.value;
	    el.setAttribute('maxlength', 11)	   
	    if(len == 3){
	       nv = '-';           
	    } else if(len == 6){
	       nv = '-';           
	    } 
	    
	    el.value = val+nv;  
	}

    validStockSymbol(el, msg){
	    var value = el.value;       
		var regex   = /^[a-zA-Z]{4,5}$/;
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

 	formatStockSymbol(el, msg, maxlen){
		el.setAttribute('maxlength', maxlen);			    
	    let val = el.value;	    
	    val = val.toUpperCase();           	    
	    el.value = val.toUpperCase(); 
	    if(true === validStockSymbol(el, msg)){
	    	return true;
	    } 
	}

    validString(el, msg, min, max){
	    var value = el.value;       
		var regex   = /^[a-zA-Z\s_-]+$/;
		if(this.withinRange(value, min, max) === false){
			this.setError(el, msg);
		    return false;
		}else 
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    validState(el, msg){
	    var value = el.value;       
		var regex   = /^[A-Z]{2}$/;
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}
	
    validAlphaNumeric(el, msg, min, max){
	    var value = el.value;       
		var regex = /^[a-zA-Z0-9 ]+$/;		
		if(this.withinRange(value, min, max) === false){		
			this.setError(el, msg);
		    return false;
		}else	
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    validStreet(el, msg, min, max){
	    var value = el.value;       
		var regex = /^[a-zA-Z0-9\s\.\#@\-%]+$/i;		
		if(this.withinRange(value, min, max) === false){
			this.setError(el, msg);
		    return false;
		}else 
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

	validAddress2(el, msg, min, max){
		var value = el.value;       
		var regex = /^[a-zA-Z0-9 \.\#@\-%]+$/i;
		if(this.withinRange(value, -1, max) === false){
			this.setError(el, msg);
		    return false;
		}else 
		if(value == ''){
			return true;
		}
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}
 	onlyNumbers(el){
		var val = el.value;
		val = val.replace(/\D/g,'');		
	}
	
    onlyNumbersAndDashes(el){
		var val = el.value;
		val = el.value.replace(/[^0-9-]/g,'');
		el.value = val;
	}
	
    onlyAllowChars(el, chars){
		var val = el.value;
		var regex = new RegExp(eval('/[^'+chars+']/'));
		val = el.value.replace(regex,'');
		el.value = val;		
	}
	
	validNumber(el, msg, min, max){
	    var value = el.value;   	   
		var regex   = /^[0-9]+$/;
		if(this.withinRange(value, min, max) === false){
			this.setError(el, msg);
		    return false;
		}else         
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    validDglNumber(el, msg, min, max){
	    var value = el.value;       
		var regex   = /^[0-9-_]+$/;
		if(this.withinRange(value, min, max) === false){
			this.setError(el, msg);
		    return false;
		}else         
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    validCompany(el, msg){
	    var value = el.value;       
		var regex   = /^[a-zA-Z0-9.\s_\-\(\)\;\\:\,\&]+$/;		
		if(regex.test(value) === false){
		    this.setError(el, msg);
		    return false;
		}else{
		    this.unsetError(el);
		    return true;
		}
	}

    validCheckbox(el, msg){
		var ischecked = el.is(':checked');
		if(!ischecked){	    
	        this.setError(el, msg, 'span');
	    }
		if(ischecked){	    
	        this.unsetError(el, 'span');		
	    }		       
	}	 
}

////////////// end v@lidator.js ///////////////////		
 