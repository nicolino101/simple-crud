"use strict"; 
	
class Utilities
{		
	static getFormObject(formElement){
		const formData = {};
  		const inputs = formElement.elements;

		for (let i = 0; i < inputs.length; i++) {
		    if(inputs[i].name!==""){
		        formData[inputs[i].name] = inputs[i].value;
		    }
		}
		return formData;
	}
	
	static serialize(formElement){		
		return JSON.stringify(Utilities.getFormObject(formElement));
	}
	
    static confirm(body, heading, btnTrueLabel, btnFalseLabel, callBackFunction) {  
		document.head.innerHTML += '<link href="/css/Utilities.css" rel="stylesheet" type="text/css">';        
		let panel = document.querySelector(".confirm-panel");
    	let panelBody = document.querySelector(".confirm-panel-body");
    	let panelHeading = document.querySelector('.confirm-panel-heading');   			
        panel.style.top = "30%";
        panel.style.left = "25%";
        panelBody.innerHTML = body;            
        panelHeading.innerHTML = heading;           
        let cbtns = document.querySelectorAll('.cbtn');
        cbtns.forEach(function(btn){
            if(btn.id == "true"){ 
            	btn.innerText = btnTrueLabel;
                btn.addEventListener('click', function(){
                    //panel.style.top = "-300px";
        			//panel.style.left = "-300px";       			
        			callBackFunction(btnTrueLabel);
                    return true;	
                }); 
            }else{  
                btn.innerText = btnFalseLabel;
                btn.addEventListener('click', function(){
                    panel.style.top = "-300px";
        			panel.style.left = "-300px";
        			callBackFunction(btnFalseLabel);
                    return false;
                });                
            }                                                        
        });       
    }	
	
	static addFlashAlert(message, type){
		let da; 
		if(!document.getElementById('div_alert')){
		    da = document.createElement('div');		    
		    da.setAttribute('id', 'div_alert');	
		    document.getElementById('content').parentElement.after(da);		
		}else{
			da = document.getElementById('div_alert');
		}		
		da.classList.add('div-alert');
		da.classList.remove('alert-success');
		da.classList.remove('alert-danger');
		da.classList.remove('alert-info');
		if(type == 'success'){
			da.classList.add('alert-success');
		}
		if(type == 'danger'){
			da.classList.add('alert-danger');
		}
		if(type == 'info'){
			da.classList.add('alert-info');
		}
		da.classList.remove('hide');
		da.innerText = message;	
		const top = document.getElementById('header').offsetTop+7;		
		da.style.top = top+'px';
		da.style.paddingTop = '10px';
		setTimeout( () => {
			Utilities.checkForFlashAlert();
		},3000);
		return true;
	}
	
	static checkForFlashAlert(){
		let flashAlert = document.querySelectorAll('.div-alert');
		flashAlert.forEach(function(fa){
			setTimeout(function(){
				fa.style.transition = 'all 2s ease-in-out'
				fa.style.position = 'absolute';
				fa.style.marginTop = '-200px';
				fa.style.opacity = 0;
				fa.classList.remove('active');
			}, 5000)
		})
	}	
	
	/************************* 
	 * AJAX post fetch api
	 *************************/
	static textPost(url, data, callBack){				
		fetch(url, {
    		method: "POST",
  			body: data,
  			headers: {
				  "Content-Type": "text/plain; charset=UTF-8"
				 }
  	    }).then(response => {			 
			return response.text();			  
		}).then(text => {
			//console.log(json)			  
			callBack(text);
  		}).catch(err => {		   
		    callBack(err, url);	    
		});	    
	}
	
	static jsonPost(url, data, callBack){		
		fetch(url, {
  			method: 'POST',
			headers: {
			    'Content-Type': 'application/json;charset=utf-8'
			},
  			body: data
		}).then(response => {
			return response.json();
		}).catch(err => {		   
		    console.log(err, url);	    
		});    
	}
	
	static load(container, url, data, type){
		Utilities.ajax(url, data, type, function(response){
			container.innerHTML = response;
		});
	}
	
	static objectToFormData(obj){
		let formData = new FormData();
		for (const [key, val] of Object.entries(obj)) {
		    formData.append(key, val);
		}
		return formData;
	}
	/************************* 
	 * AJAX get fetch api
	 *************************/
	 static get(url, type, callBack){
		fetch(url, {
    		method: 'get',    		
  		}).then(response => {
			  if(response.ok){
				  return type == 'text' ? response.text() : response.json();
			  }else{
				  return response.text();
			  }
		}).then(json => {
			  //console.log(json)			  
			  if(json.status == 'fail'){
	              Utilities.handleErrors(json, data);
	          }else
	          if(json.status == 'success'){
				  callBack(json);
	          }else{
				  //console.log(json); 
				  callBack('..ERROR..');
			  }
  		}).then(text => {
			  //console.log(text)			  
			  
  		}).catch(err => {		   
		    //console.log(err, url);	    
		})	    
	}
	
	/************************* 
	 * AJAX post XMLHttpRequest
	 *************************/
	static ajax(url, data, type, callBack){		
		let xhr = new XMLHttpRequest();
	    xhr.open("POST", url);
	    
	    if(type === 'json'){
	    	xhr.setRequestHeader("Content-type", "application/json");
	    }
	    
	    if(type === 'text'){
	    	xhr.setRequestHeader("Content-type", "text/plain");
	    }
	    
	    xhr.onload = () => {
	        let response = xhr.response;
	        //console.log(response)
	        callBack(response);           	
	    }   
	      	    
	    xhr.send(data);
	}
}

window.addEventListener('load', function(){ 
    Utilities.checkForFlashAlert();
});

