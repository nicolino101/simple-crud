<?php

spl_autoload_register(function($className) {
    
    $className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    $file = $_SERVER['DOCUMENT_ROOT'] . '/../' . DIRECTORY_SEPARATOR . $className . '.php';
    if(file_exists($file)){       
        require_once $file;
    }
});