<h2>Simple PHP CRUD Sample</h2>

<h3>DESCRIPTION</h3>
<p>
PHP 8 OOP MVC CRUD - Utilizes a very simple MVC (Model View Controller) type design pattern. 
</p>

<h3>Basic Flow & Features</h3>
<p>
Front controller pattern routing all $_REQUEST to the /public/index.php, autoloads what it needs and moves on the a basic Router.
</p>
<p>
Router creates a Request Object and injects into appropriate Controllers via the AbstractController. 
</p>
<p>
You can create a new View() in controller methods, and add variables. Variables get Sanitized in the View Model before hitting the client side. You can also set the layout(/application/layout...)
UserController has all the CRUD functions (Create Read Update and Delete ++) as examples.
</p>
<p>
The controller calls a Validator passing post params to be validated.
</p>
<p>
User Model .... and blah, blah, blah.
</p>
<p>
You'll also find some cool tools in /application/service directory...Flash, Redirect, Router, View, AbstractController and other services not used yet. 
</p>
<b>Front End</b><br>
<p>
<ul>
<li>index.php is page with a responsive table showing all users created. You can click on edit or delete on this page.</li>
<li>contact.php simple little slide in contact form sample.</li>
<li>Front End /application/user/view includes and index.php, contact.php and a form.php.</li>
<li>form.php is an interactive form page for creating and updating users.</li>
</p>
<p>Utilizes flexbox CSS and Vanilla JS and is fully responsive.</p>
<p>
The form integrates google places, geometry, geocoding for autocompletion of address fields.
custom Vanilla JavaScript Validator as well as a PHP Validator for the back end.
Sanitizes All Data in and out automatically.
</p>
<p>
Database portion uses a PDO Connection with prepare statements to protect against SQL Injection.
Flash Messages included and more....
</p>
<p>
MySql Schema provided with sample inserts.
</p>

<h3>DEPENDENCIES</h3>
<ul>
<li>The only dependencies are font-awesome and all inclusive in this repository.</li>
<li>AND you'll need a free google places API_KEY application/config/Config.php replace YOUR_API_KEY with your key you get from google. <a href="https://developers.google.com/maps/documentation/javascript/get-api-key">Click HERE TO Start the process of getting the GOOGLE API KEY</a></li>
<li>NO Composer Needed for dependencies...(yet)</li>
</ul>

<h3>INSTALLATION</h3>
<ol>
<li>git clone https://gitlab.com/nicolino101/simple-crud.git or download the zip</li>
<li>extract to a directory that is exposed to your webserver...like /var/www/html/???
   C:\xampp\htdocs....</li>

<li>I use a Linux OS and NGINX web server so Root is usually /var/www/html/....  but use whichever you like even PHP built-in will work XAMPP, WAMP, etc...
<br><b>just make sure to make the DOCUMENT_ROOT /public folder and route all to index.php</b></li>

<li>You'll need to create the table and inserts for mysql which I've included...located in application/database/schema.
</li>
<li>Also configure the Db in /application/config/Config.php ... username, password, databasename
<li>I've moved all configuration to /application/config/Config.php to get you up and running easier.<br>
    So the google api key will be entered in there any company information.</li>
<li>You can add any configs in there, it's just a basic Registry type design.
    call connection to db like: Db::getDb('databasename');
    the Db Model then grabs the params from the Config file like ... 
	<dl>
	    <dt>Config::get('db', 'crud'); static function returns all params in an array.</dt>
		<dd>Google api key is Config::get('api', 'google')['api_key'];</dd>
		<dd>Company params are fetched like.... Config::get('company', 'info')['title']</dd>
		<dd>$company = Config::get('company', 'info');</dd>
		<dd>and $company is a array of all the params like phone, address, logo, whatever...</dd>
	</dl>
	</li>
</ol>

<p>
note:<br>
ROUTER and Routes already set.<br>
PHP 8 > Recommended PHP 8.2 ><br>
Mysql 5.7 > Recommended 10.6.12-MariaDB<br>
</p>


<h3>HERE is my nginx config file for this project</h3>

<pre>

server {
	
	listen 80;
	
	listen [::]:80;

	index index.php index.html;

    # if you add - localhost  crud.local to your hosts file you can call by http://crud.local in browser
    # linux - add to /etc/hosts | windows c:\Windows\System32\Drivers\etc\hosts
	server_name crud.local;		
	
	gzip on;
	
	access_log  /var/log/nginx/crud.local.access.log;
	error_log   /var/log/nginx/crud.local.error.log; 
	
	client_max_body_size 128M;
			
	location / {
	    try_files $uri $uri/ /index.php;	
	}

	location ~ \.php$ {
	    include snippets/fastcgi-php.conf;
	    fastcgi_pass unix:/run/php/php8.2-fpm.sock;	    
	    fastcgi_param   PATH_INFO         $fastcgi_path_info;
	    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
	    include        fastcgi_params;
	    fastcgi_max_temp_file_size 0;
	    fastcgi_buffer_size 32K;
	    fastcgi_buffers 32 64k;
	    fastcgi_send_timeout 300;              
	    include        fastcgi.conf;
	}

	 # A long browser cache lifetime can speed up repeat visits to your page
	location ~* \.(jpg|jpeg|gif|png|webp|svg|woff|woff2|ttf|css|js|ico|xml)$ {
	    access_log        off;
	    log_not_found     off;
	    expires           360d;
	}
        
	# disable access to hidden files
	location ~ /\.ht {       
	    access_log off;
	    log_not_found off;
	    deny all;
	}
	
	## DOCUMENT_ROOT	
    root  /var/www/html/crud.local/public/;
}

</pre>
